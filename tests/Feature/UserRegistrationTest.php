<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserRegistrationTest extends TestCase
{
    // TODO : Repair test
    // public function testRegisterUserWithValidInput()
    // {
    //     $response = $this->post('/api/register',
    //                                 ['name' => 'Mick Jones',
    //                                 'email' => 'mickjones@theclash.com',
    //                                 'password' => 'password'],
    //                                 ['Accept' => 'application/json']);
    //     $response->assertJsonFragment(['name' => 'Mick Jones',
    //                                     'email' => 'mickjones@theclash.com']);
    //     $response->assertJsonStructure(['data' => ['id', 'name', 'email', 'token']]);
    //     $response->assertStatus(201);
    // }

    public function testRegisterUserWithNonStringName()
    {
        $response = $this->post('/api/register',
                                ['name' => 3,
                                'email' => 'mickjones@theclash.com',
                                'password' => 'password'],
                                ['Accept' => 'application/json']);
        $response->assertJsonFragment(["name" => ["The name must be a string."]]);
        $response->assertStatus(422);
    }

    public function testRegisterUserWithInvalidEmail()
    {
        $response = $this->post('/api/register',
                                ['name' => "Mick Jones",
                                'email' => 'not_an_actual_email',
                                'password' => 'password'],
                                ['Accept' => 'application/json']);
        $response->assertJsonFragment(["email" => ["The email must be a valid email address."]]);
        $response->assertStatus(422);
    }

    public function testRegisterUserAnAlreadyExistingEmail()
    {
        $response = $this->post('/api/register',
                                ['name' => "Mick Jones",
                                'email' => 'admin@api.com',   //Email already assigned in User seeder
                                'password' => 'password'],
                                ['Accept' => 'application/json']);
        $response->assertJsonFragment(["email" => ["The email has already been taken."]]);
        $response->assertStatus(422);
    }

    public function testRegisterUserWithMissingName()
    {
        $response = $this->post('/api/register',
                                ['email' => 'mickjones@theclash.com',
                                'password' => 'password'],
                                ['Accept' => 'application/json']);
        $response->assertJsonFragment(["name" => ["The name field is required."]]);
        $response->assertStatus(422);
    }

    public function testRegisterUserWithMissingEmail()
    {
        $response = $this->post('/api/register',
                                ['name' => "Mick Jones",
                                'password' => 'password'],
                                ['Accept' => 'application/json']);
        $response->assertJsonFragment(["email" => ["The email field is required."]]);
        $response->assertStatus(422);
    }

    public function testRegisterUserWithMissingPassword()
    {
        $response = $this->post('/api/register',
                                ['name' => 3,
                                'email' => 'mickjones@theclash.com'],
                                ['Accept' => 'application/json']);
        $response->assertJsonFragment(["password" => ["The password field is required."]]);
        $response->assertStatus(422);
    }
}
