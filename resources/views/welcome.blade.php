<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Rest API
                </div>

                <div class="links">
                    <a href="https://atomrace.com/tutoriels-sur-laravel-5-5-comment-creer-une-api-rest-robuste-et-la-deployer-sur-le-web/">Atomrace.com</a>
                    <a href="https://atomrace.com/laravel-creer-une-api-rest-avec-laravel-5-5/">API REST</a>
                    <a href="https://atomrace.com/robustesse-de-votre-controleur/">Contrôleur</a>
                    <a href="https://atomrace.com/definir-les-relations-entre-les-modeles-orm-eloquent/">ORM</a>
                    <a href="https://atomrace.com/authentification-et-autorisation-avec-passport-pour-laravel/">Passport</a>
                    <a href="https://auth0.com/docs/quickstart/webapp/laravel/01-login">Auth0</a>
                    <a href="https://atomrace.com/deployer-votre-projet-laravel-5-5-sur-heroku/">Heroku</a>
                </div>
                <div class="links">
                <div class="links"><br></div>
                    <a href="https://laravel.com/docs">Doc</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
    </body>
</html>
