<?php

namespace App\Http\Controllers;

use App\Measure;
use App\Station;
use Carbon\Carbon;
use App\Http\Resources\MeasureResource;
use App\Http\Requests\MeasurePostRequest;
use Illuminate\Database\Eloquent\Collection;

class MeasureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return MeasureResource::collection(Measure::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(MeasurePostRequest $request, Station $station)
    {
        $measure = new Measure();
        $measure->value = $request->input('value');
        $measure->description = $request->input('description');
        $measure->station_id = $station->id;
        $measure->save();

        return new MeasureResource($measure);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Measure $measure
     * @return \Illuminate\Http\Response
     */
    public function show(Measure $measure, Station $station)
    {
        $measures = Measure::where('station_id', $station->id)->get();

        if($measures->isEmpty()) {
            $json = ["message" => "Station has no measure."];
            return response($json, 200);
        }

        $response = new Collection();
        for($i=0; $i < $measures->count(); $i++)
        {
            $response[$i] = new MeasureResource($measures[$i]);
        }

        return $response;
    }

    /**
     * Display measures from station with color index
     */
    public function get24h(Station $station)
    {
        $fromDate = Carbon::now()->subHours(24);
        $toDate = new Carbon('now');

        $measuresCollection =
            Measure::whereBetween('created_at',
                array($fromDate->toDateTimeString(),
                    $toDate->toDateTimeString()))
                ->where('station_id', $station->id)
                ->get();

        if($measuresCollection->isEmpty()) {
            $json = ["message" => "Station has no measure."];
            return response($json, 200);
        }

        $measuresGrouped = $measuresCollection->groupBy('description');

        return $measuresGrouped;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Measure  $measure
     * @return \Illuminate\Http\Response
     */
    public function update(MeasurePostRequest $request, Measure $measure)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Measure  $measure
     * @return \Illuminate\Http\Response
     */
    public function destroy(Measure $measure)
    {
       //
    }
}
